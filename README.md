# How to read this repository

## What am I looking at?! 
At the moment you are looking at what is called a 'repository'. A repository is a collection of files, mostly used in programming and software development to collect all scripts related to a software project. A software repository is generally tracked through [git](https://git-scm.com/). If you want to learn more about git and how to use it, [this tutorial used in the course Geoscripting](https://geoscripting-wur.github.io/RProjectManagement/) contains information about git and how to use it. Scroll down to version control to read more. For now, it is enough to know that it is a way to distribute files and code, and it is likely to come across git repositories during this course. 

The file you are looking at right now is called the 'readme'. It is a file that contains a description of a repository and it includes some context to make sense of the code. We will use this file as a reading guide for the notebooks and other study materials that will be provided. 

## What is in this repository?

## Program
Below a program will be _suggested_. Since the starting levels are varying, this program is the recommended program, feel free to divert from it as you wish. If you are already familiar with programming, or if learning to program is one of your personal learning goals, you might take on a bit more work. If you want to focus more on something else, feel free, there are no mandatory exercises to hand in.

### Schedule
During this course we will spend 3 full days in which you will get familiar with processing data in Python, cleaning the data to make it ready for analysis and apply some machine learning tools. These 3 days will be split in 3 different themes: 

- Python Day 
- Data Day
- Machine Learning Day

The goal is that after these three days you will be able to use python in your project for data analysis. It is good to stress that the goal is not to educate you as a software developer or data engineer, there are other courses for that, (and we will happily [borrow course material from those](https://geoscripting-wur.github.io/)), but rather, you will be able to understand and use out of the box tools within the python community and make that work for you. 

The goals are subdivided among the three days as follows 

- Python Day  _Wednesday, January 10th 2024_
  - Introduction to python
  - Basic python and related technology (markdown, google colab)
  - Python and open source 
  
- Data Day _Monday, January 15th 2024_
  - Explore the data using Pandas and Seaborn
  - Pandas
  - Databases

- ML Day _Thursday, January 18th 2024_
  - TBA

## Course material
The material for these days will be provided through the repository you are reading this from. If you are reading this you found it: well done! You can download all material using the download button and clicking the `zip` format, or separate files using the download button in the separate file, the files are living files, and they might change throughout the course. It is therefor recommended to download the files separately.
![download files](../imgs/download-from-git.png)

The repository will contain a few files including:
- `.md` file 
  - This is what you are reading right now, and is best to be read using the browser on https://git.wur.nl/ds-smartenv/python-intro. md stands for [Markdown](https://www.markdownguide.org/getting-started/), it is a text editing language that is very lightweight and commonly used in the scripting (and data science!) community. 
- `.ipynb` files
  - This stands for Python Notebook files. Python notebooks are interactive python scripts mixing text (in markdown),(python) code and visualization. We will dive in python notebooks later. 

This `README.md` file contains the reading guide and the basic instructions. Practical instructions on python will be available through the Jupyter Notebooks (`.ipynb`) files.  

