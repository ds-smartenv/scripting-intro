# Welcome to pythonday!

![Python](../imgs/python.png)

## Introduction
In this course (Data Science for Smart Environments) we are going to try to take advantage of the data produced by Smart Environments. Smart Environments produce multiple different types of data and often a lot of it in a small amount of time, and possibly in real time. Raw data that is produced is formatted in a way that is readable by computers, and even when read by a human the data contains raw numbers that need to be processed and manipulated into knowledge that can be used do something meaningful with. This process of turning raw numbers into information that can be understood by people that need this information to make a decision, is an important task of a data scientist.  

![img.png](../imgs/data-info-action.png) 

*[source](https://www.tumblr.com/informationversusknowledge-blog/113822495622/created-by-bruce-campbell-dika-ancient-chinese) (Can anybody confirm or deny that [DIKA means 'get up and go'?](https://www.google.com/search?q=what+does+dika+mean+in+chinese&sca_esv=595745443&rlz=1C1GCFG_enNL1087NL1087&sxsrf=AM9HkKkw95jHtwrd-7cVdu1zCHbsCn_tcQ%3A1704399057018&ei=0RCXZfdRuZX27w_e-5LYBA&ved=0ahUKEwi3_uSixcSDAxW5iv0HHd69BEsQ4dUDCBA&uact=5&oq=what+does+dika+mean+in+chinese&gs_lp=Egxnd3Mtd2l6LXNlcnAiHndoYXQgZG9lcyBkaWthIG1lYW4gaW4gY2hpbmVzZTIHECEYChigAUiqLVDmBVi_LHADeAGQAQCYAZkBoAGxEqoBBDIyLjW4AQPIAQD4AQHCAgoQABhHGNYEGLADwgIEECMYJ8ICBRAAGIAEwgIREC4YrwEYxwEYywEYgAQYjgXCAgsQLhivARjHARiABMICCBAuGIAEGMsBwgIOEC4YgAQYywEYxwEYrwHCAggQABiABBjLAcICDhAuGIAEGMcBGK8BGI4FwgIGEAAYBxgewgIIEAAYBxgeGArCAggQABgHGB4YD8ICCxAAGIAEGIoFGIYDwgIIEAAYCBgHGB7CAgoQABgIGAcYHhgPwgIIEAAYCBgeGA_CAggQABgIGB4YDcICCRAAGIAEGA0YCsICBxAAGIAEGArCAggQABgWGB4YCsICCBAAGBYYHhgPwgIKEAAYFhgeGA8YCsICBhAAGBYYHsICCRAhGAoYoAEYCuIDBBgAIEGIBgGQBgY&sclient=gws-wiz-serp#ip=1))*

## Raw data to information
The large quantity and velocity of data requires handling the data in specific ways. There are many different ways to manipulate data, for example it is possible to use [hardware](https://www.youtube.com/watch?v=-M6lANfzFsM) to do complicated calculations, but a more modern approach is to use computers. Computers are very useful and reliable in repeating a set of calculations designed by a specialist, but it can be difficult to instruct the computer to do what you want. Programming languages are the common language between humans and computers. One of these languages, and the one that we will use throughout this course, is Python. Today you will learn the basics and the prerequisite skills you will need to manipulate and process data using Python. Python is one of the favorite tools of data scientists, and it has been one of the [most popular languages](https://pypl.github.io/PYPL.html) in recent years. The most cited [reason](https://github.blog/2023-03-02-why-python-keeps-growing-explained/#:~:text=Python%20is%20a%20popular%20language,applications%20were%20built%20with%20Python.) for this popularity is that it is relatively easy to read, write and its flexibility.

## Python 
Python is an interpreted language, ([as opposed to compiled programming languages](https://www.freecodecamp.org/news/compiled-versus-interpreted-languages/)) and in its most basic form you can type commands in a terminal line by line and running them separately. While this works, a more practical way of using python is by writing scripts and combining multiple lines in a script, which will be interpreted in its entirety. There are tools that can help you to write scripts called Integrated Development Environments (IDE). The most basic ones will do nothing more than help edit the scripts while others will help you with everything including spotting coding errors and renaming variables and functions throughout an entire project (these can become quite complicated to use).  

During this course we will use jupyter notebooks in combination with [Google Colab](https://colab.research.google.com/).

### Jupyter notebooks

[Jupyter notebooks](https://jupyter.org/) are browser-based notebooks for Julia, Python, and R, they correspond to `.ipynb` files. They are used often by data scientists because of a few reasons
* In-browser editing for code, with automatic syntax highlighting, indentation, and tab completion/introspection. This also allows easy usage of powerful (cloud) resources, for example googles platform.
* The ability to execute code from the browser and plot interactive and inline visualizations.
* In-browser editing for rich text using the Markdown markup language.

Notebook documents contain the inputs and outputs of an interactive python shell as well as additional text that accompanies the code but is not meant for execution. In this way, notebook files can serve as a complete computational record of a session, interleaving executable code with explanatory text, mathematics, and representations of resulting objects. These documents are saved with the `.ipynb` extension. Notebooks may be exported to a range of static formats, including HTML (for example, for blog posts), LaTeX, PDF, etc. by `File->Download as`

### Alternatives
Another option that is commonly used in Data Science, is [Anaconda](https://www.anaconda.com/products/individual-d). You can use Anaconda to download and install Python and its environment. (see also the [quickstart](https://docs.anaconda.com/anaconda/user-guide/getting-started/)). It might be beneficial to use Anaconda and a local python installation during the project. Contact the staff if you think you need this and do not know how to set it up. If you are already used to another python IDE, feel free to use that. 

## The real work! 
Suggested material for today: 

[the introduction to python](python-day/1_intro_to_python_and_colab.ipynb). This notebook contains explanations about the following topics: 
- Google Colab
- Markdown
- Basic Python 
  - Variables
  - Data types
  - Basic operations 
  - Lists, tuples, dictionaries and related operations 
  - Strings 
- Branches and loops
- Functions
  - Defining them
  - returning something from functions
  - Arguments
- External libraries and open source

In the end there are some basic exercises regarding these topics. 

1. Go to [colab.research.com](https://colab.research.google.com/)
2. Upload the notebook and follow the instructions!

## Some extra materials 
- [Introduction to Python for Data Sciences by Frank Iutzler](https://github.com/iutzeler/Introduction-to-Python-for-Data-Sciences/tree/master) - This contains several notebooks about Python and related libraries that are useful for data scientists. Chapters that are recommended to follow are
  1. [Basics](https://github.com/iutzeler/Introduction-to-Python-for-Data-Sciences/blob/master/1_Basics.ipynb)
  2. [Pandas](https://github.com/iutzeler/Introduction-to-Python-for-Data-Sciences/blob/master/3-1_Pandas.ipynb) and [Dataframes](https://github.com/iutzeler/Introduction-to-Python-for-Data-Sciences/blob/master/3-2_Dataframes.ipynb). The [NumPy](https://github.com/iutzeler/Introduction-to-Python-for-Data-Sciences/blob/master/2_Numpy_and_co.ipynb) tutorial is being referenced to in these. 
  3. [Fancy visualization](https://github.com/iutzeler/Introduction-to-Python-for-Data-Sciences/blob/master/3-3_Fancy_Visualization_with_Seaborn.ipynb)

