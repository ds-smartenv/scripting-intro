# Data day! 

Welcome to data day!

## Options!
For today practicals there are not one but several notebooks. They contain the basics on some topics that you are likely to need during the project. For today, you might want to pick a single topic, start with that and continue learning using suggested online materials. Another option would be to study all the basics. A choice between broad or deep knowledge. 

## Intro to pandas 
If you want to learn more about Pandas, [this notebook](./data_day/2_intro_pandas.ipynb) will cover a dive into the basics. If you are curious, and want to learn more, the website from [Tomas Beuzen](https://www.tomasbeuzen.com/python-programming-for-data-science/README.html) contains a large collection of tutorials about data science and python. This introduction is based on [chapter 7](https://www.tomasbeuzen.com/python-programming-for-data-science/chapters/chapter7-pandas.html) and [chapter 8](https://www.tomasbeuzen.com/python-programming-for-data-science/chapters/chapter8-wrangling-basics.html).

## Data Wrangling
The notebook [intro to data wrangling](2_data_wrangling.ipynb) is a notebook where some simple data wrangling and cleaning techniques will be introduced. Make sure that you studied the basics of Pandas before. [Chapter 9](https://www.tomasbeuzen.com/python-programming-for-data-science/chapters/chapter9-wrangling-advanced.html) from [Tomas Beuzen](https://www.tomasbeuzen.com/python-programming-for-data-science/README.html) is a good continuation if you want to learn more.

## Intro to apis
Several groups talked about accessing API's for their project. If you want to learn more about accessing data through API
s, [the introduction to api's](2_intro_apis.ipynb) is a good start. This tutorial is very loosely based upon [this tutorial](https://realpython.com/python-api/), however it is simplified a bit. If you are still curious read through the original tutorial. 

## Intro to databases and SQL 
Databases are useful tools to store data and manipulate it quickly. If you want to learn more about them, start with reading [this blog post](https://medium.com/@rwilliams_bv/intro-to-databases-for-people-who-dont-know-a-whole-lot-about-them-a64ae9af712). If you want to practice with SQL and learn how to access databases from python and jupyter notebooks, follow the tutorials in [the databases folder](./databases). Read the [readme](./databases/README.md) Start with the first 3 exercises (ex00 - ex03). If you want to learn even more start going throught the other exercises.